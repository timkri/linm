# -*- coding: utf-8 -*-
import os, sys
sys.path.append("%s/*" % sys.argv[0])

import time, math, exceptions, traceback
from itertools import cycle
from sets import Set

import org.sikuli.natives.OCR as OCR

from datetime import datetime
from threading import Thread
from java.util.concurrent.locks import ReentrantLock
from java.util.concurrent.atomic import AtomicBoolean
from java.util.concurrent.atomic import AtomicInteger
from java.awt import Toolkit
from java.lang import System

def log(message):
    System.out.println("[%s][log] %s" % (datetime.now().strftime("%Y-%m-%d %H:%M:%S"), message))

def reset(varName):
    try:
        globals()[varName] = None
    except: pass

GLOBAL_VARS = ["SUFFICIENT_HP_THRESHOLD", "PVP_UNDER_ATTACK_BACK_HOME", "AUTO_ATTACK_USE_KEY", "AUTO_HEALTH_RECOVERY_FORCE_TIMES", "AUTO_TELEPORT_HOTKEY", "AUTO_MANA_RECOVERY_THROTTLER", "CHARACTER_MAX_MANA", "AUTO_ATTACK_USE_KEY", "AUTO_HEALTH_RECOVERY", "AGGRESIVE_ATTACKING", "COUNTER_ATTACK", "EXCLUSIVE_MODE", "ATTACK_PERIOD_SECONDS", "SUFFICIENT_MP_THRESHOLD", "AUTO_TELEPORT_ENABLED", "AUTO_TELEPORT_ENABLED_INTERNAL", "TO_DREAM_ISLAND_TIME", "TO_DREAM_ISLAND_TIME_ON_MAINTAIN_DAY", "AUTO_HEALTH_HOTKEY"]
[reset(x) for x in GLOBAL_VARS]

#find("1519537736861.png")

FAST_RECOVERY_MANA = {
    "keys": ["2", "2", "6", "6"],
    "delays": [2, 0.5, 0.5, 0.5]
}
FAST_RECOVERY_BOSS = {
    "keys": ["6"],
    "delays": [1]
}
FAST_BUY_BOXES = {
#    "keys": ["1520267016692.png", "1520267047658.png", "5"],
    "keys": ["1520267252461.png", "1520267047658.png", "5"],
#    "keys": ["1520267292507.png", "1520267047658.png", "5"],
    "delays": [0.8, 0.8, 0.8]
}

FAST_RECOVERY = False
#FAST_RECOVERY_PROC = FAST_RECOVERY_MANA
FAST_RECOVERY_PROC = FAST_RECOVERY_BOSS
#FAST_RECOVERY_PROC = FAST_BUY_BOXES

# HP 自動高治 - 當魔力大於 SUFFICIENT_MP_THRESHOLD，且血量小於 SUFFICIENT_HP_THRESHOLD
AUTO_HEALTH_RECOVERY = False
AUTO_MANA_RECOVERY = False
AUTO_TELEPORT_ENABLED = False
PVP_COUNTER_ATTACK = False

#=============================
AUTO_TELEPORT_HOTKEY = "1522194554295.png"
AUTO_HEALTH_HOTKEY = "1518849282451.png"
#AUTO_HEALTH_HOTKEY = "6"
AUTO_HEALTH_RECOVERY_FORCE_TIMES = 12
AUTO_MANA_RECOVERY_HOTKEY = "2"
AUTO_MANA_RECOVERY_THROTTLER = 4
AUTO_ATTACK_USE_KEY = None

SUFFICIENT_HP_THRESHOLD = 1000
SUFFICIENT_MP_THRESHOLD = 300

# 積極攻擊 - 放三重矢
AGGRESIVE_ATTACKING = True
AGGRESIVE_ATTACKING_FORCE = False

ATTACK_PERIOD_SECONDS = 4
AUTO_TELEPORT_HOTKEY = None

# 反擊模式
PVP_DISABLED = False
PVP_UNDER_ATTACK_BACK_HOME = False

GAME_RECORDS_DIR = "/Users/timchang/Documents/LinM"
PVP_ATTACKED_RECORDS_DIR = "%s/attacked" % GAME_RECORDS_DIR

# 自動到遺棄者的區域
TO_ABANDONED_AREA_TIME = 18010
#TO_ABANDONED_AREA_TIME = 0
#TO_ABANDONED_AREA_TIME = -1
TO_ABANDONED_AREA_TIME_ON_MAINTAIN_DAY = 57600
#TO_ABANDONED_AREA_TIME_ON_MAINTAIN_DAY = 57600 # 16:00

TO_DREAM_ISLAND_TIME = 25260
TO_DREAM_ISLAND_TIME_ON_MAINTAIN_DAY = -1

def prepareCounterAttack():
    type("7")
    
def quickPVPAttack():
    type("1")
    type("6")

# =========DO NOT CHANGE=========

def noneIfUndef(varName):
    try:
    	return eval(varName)
    except:
        return None

# Constants and Settings
CHARACTER_MAX_MANA = 1000
DAY_IN_SECS = 86400
HOUR_IN_SECS = 3600
TZOFFSET = 28800
MAINTENANCE_DAY = 3
EXCLUSIVE_MODE = AtomicBoolean(False)
APP_EXITING = AtomicBoolean(False)
AUTO_TELEPORT_ENABLED_INTERNAL = AtomicBoolean(AUTO_TELEPORT_ENABLED)

#OCR.setParameter("classify_bln_numeric_mode", "1")
OCR.setParameter("tessedit_char_whitelist", "0123456789")

def resetDelay():
    Settings.MoveMouseDelay = 0
    Settings.DelayBeforeMouseDown = 0
    Settings.ClickDelay = 0
    Settings.TypeDelay = 0
    Settings.KeyPressDelay = 0
    Settings.KeyReleaseDelay = 0

def exclusiveActions(action):
    if not EXCLUSIVE_MODE.compareAndSet(False, True): return
    try:
        action()
    finally:
        EXCLUSIVE_MODE.set(False)

def checkIfAppExiting(raiseError=True):
    if not APP_EXITING.get(): 
        return False
    raise Exception("System shutdown") if raiseError else True

def detectPatternThenTakeAction(pics, takeAction, similarity=0.8):
    m = None
    for ps in pics:
        try:
            m = find(ps)
            if m.getScore() >= similarity: 
                if takeAction: takeAction()
                break
            elif m.getScore() > 0.5:
                # Keep guessing
                continue
            else: break
        except FindFailed: break
    return 0 if not m else m.getScore()

def waitAndClick(pic):
    try:
        wait(pic)
        return click(pic)
    except FindFailed: return False

def captureScreenshot():
    return capture(getBounds())
    
def makeWarningSound(times):
    def makeBeep():
        for i in range(times):
            Toolkit.getDefaultToolkit().beep()
            time.sleep(0.3)

    Thread(target=makeBeep).start()

def performActionUntilStop(action, stopCond, checkStopSecs=2):
    lastCheckTime = time.time()
    while True:
        if action: action()
        checkNow = time.time()
        checkElapsed = checkNow - lastCheckTime
        if checkElapsed > checkStopSecs: 
            if stopCond(): break
            lastCheckTime = checkNow

global mouseActionLock
mouseActionLock = ReentrantLock()
def clickIfExists(pic):
    mouseActionLock.lock()
    try:
        resetDelay()
        return click(pic)
    except FindFailed: return False
    finally: mouseActionLock.unlock()

def throttler(periodSecs):
    lastTime = [time.time()]
    def routine():
        if periodSecs <= 0: return True
        now = time.time()
        elapsed = now - lastTime[0]
        if elapsed >= periodSecs:
            lastTime[0] = now
            return True
        return False
    return routine

keyThrottlers = dict()
def pressKey(key):
    try: 
        if not key in keyThrottlers:
            keyThrottlers[key] = throttler(0.5)

        while not keyThrottlers[key](): return
        
        resetDelay()
        keyDown(key)
    finally: 
        keyUp(key)
        pass

def backgroundThread(busyRoutine, name=""):
    APP_STOPPED = AtomicBoolean(False)

    def routine():
        if not busyRoutine: return

        try:
            while not APP_STOPPED.get():
                if busyRoutine():
                    log("[%s] The background routine choose to exit..." % name)
                    break
        except Exception as e:
            log("[%s] An error has occured: %s" % (name, e))
            raise
        finally:
            log("[%s] The BACKGROUND thread stopped. APP_STOPPED=%s" % (name, APP_STOPPED.get()))
    
    backgroundThread = Thread(target=routine)
    backgroundThread.start()

    def cancel():
        try:
            APP_STOPPED.set(True)
            backgroundThread.interrupt()
            #backgroundThread.join()
        except: pass
    return lambda: cancel()

class SubStories:
     
    def __init__ (self):
        self.goneToDreamIsland = False
        self.goneToAbandonedArea = False

    def goToDreamIsland(self):
        if self.goneToDreamIsland: return
        self.goToMap(["1520155109770.png"])
        self.goneToDreamIsland = True
    
    def goToAbandonedArea(self):
        if self.goneToAbandonedArea: return
        self.goToMap(["1518648980956.png"])
        self.goneToAbandonedArea = True

        
    def goToMap(self, pics):
        try:
            REGION = Region(56,267,207,408)
            anchor = find("1516631988582.png")
            click(anchor.above(50))

            lastMatchedPic = [x for x in pics if REGION.exists(x)]
            while not lastMatchedPic:
                m = REGION.find("1516626141423.png")
                hover(m)
                mouseDown(Button.LEFT)
                wait(0.5)
                mouseMove(m.getCenter().below(200))
                mouseUp(Button.LEFT)
                lastMatchedPic = [x for x in pics if REGION.exists(x)]


            if clickIfExists(lastMatchedPic[0]):
                waitAndClick("1516627102272.png")
            wait(1)
            clickIfExists("1516628395113.png")
            AUTO_TELEPORT_ENABLED_INTERNAL.set(True)
        except FindFailed: pass
        finally:
            clickIfExists("1516621398337.png")


class GamingReactor:
    
    def __init__ (self):
        self.backToVillage = False
        self.lastAttackPerformed = 0

    def teleportIfNoAttackTarget(self):
        pass

    def performManaRecover(self):
        if AUTO_MANA_RECOVERY_HOTKEY:
           pressKey(AUTO_MANA_RECOVERY_HOTKEY) 
        else: clickIfExists("1520417126159.png")
    
    def performPoisonCureIfNeeded(self):
        detectPatternThenTakeAction(["1515673556328.png"], lambda: self.performPoisonCure())
        

    def performZombieCureIfNeeded(self):

        detectPatternThenTakeAction(["1515823929553.png", "1515825181000.png", "1515677390001.png", "1515678658343.png"], lambda: self.performZombieCure())

    def performPoisonCure(self):
        if clickIfExists("1515385381615.png"): time.sleep(1)

    def performZombieCure(self):
        if clickIfExists("1515385402230.png"): time.sleep(1)
    
    def performRecovery(self):
        if AUTO_HEALTH_HOTKEY: 
            if AUTO_HEALTH_HOTKEY.isdigit():
                pressKey(AUTO_HEALTH_HOTKEY)
            else: clickIfExists(AUTO_HEALTH_HOTKEY)
        elif clickIfExists("1515245792422.png"): pass

    def goBackToVillage(self):
        if self.backToVillage is False:
            clickIfExists("1515311985521.png")
            self.backToVillage = True

    def goBackToClanHome(self):
        if not clickIfExists("1516895998569.png"):
            self.teleport()

    def teleport(self):
        if AUTO_TELEPORT_HOTKEY: 
            if AUTO_TELEPORT_HOTKEY.isdigit():
                pressKey(AUTO_TELEPORT_HOTKEY)
            else: clickIfExists(AUTO_TELEPORT_HOTKEY)
        elif clickIfExists("1516896036883.png"): time.sleep(1)

    def performAttack(self, force=False):
        if self.backToVillage: return

        now = time.time()
        elapsedSecs = now - self.lastAttackPerformed
        if force or (elapsedSecs > ATTACK_PERIOD_SECONDS):
            self.lastAttackPerformed = now
            if AUTO_ATTACK_USE_KEY: 
                if AUTO_ATTACK_USE_KEY.isdigit():
                    pressKey(AUTO_ATTACK_USE_KEY)
                else: clickIfExists(AUTO_ATTACK_USE_KEY)
            elif clickIfExists("1516406758793.png"): pass

    # Unstable
    def goBackHomeWhenNoCurePotion(self):
        score = detectPatternThenTakeAction(["1516510409141.png"], lambda: self.goBackToVillage(), similarity = 0.9)
        log("NoCurePotion: %f" % score)

    def escapeIfUnderAttack(self):
        
        BATTLE_REGION = Region(1018,503,205,113)
        def saveAttackRecord(screenshotFile, suffix=False):
            attackedRecord = "%s/%s" % (PVP_ATTACKED_RECORDS_DIR, os.path.basename(screenshotFile))
            if suffix: attackedRecord = attackedRecord + suffix
            os.rename(screenshotFile, attackedRecord)

        def pvpRoutine(action):
            screenshot = None
            try:
                #makeWarningSound(3)
                now = datetime.now()
                screenshot = captureScreenshot()
                log("Attacked at %s. See screenshot: '%s'" % (now.strftime("%Y-%m-%d %H:%M:%S"), screenshot))
                try: exclusiveActions(lambda: action())
                except: pass
            finally:
                if screenshot: saveAttackRecord(screenshot)

        def isAttacking():
            return BATTLE_REGION.exists("1516895773203.png")

        if PVP_DISABLED: return

        attacking = isAttacking()
        if attacking: 
            if not PVP_COUNTER_ATTACK:
                def escape():
                    if PVP_UNDER_ATTACK_BACK_HOME:
                        self.goBackToClanHome()
                    else: self.teleport()
                pvpRoutine(lambda: performActionUntilStop(lambda: escape(), lambda: not BATTLE_REGION.exists("1516895773203.png"), 1))
                return
            else:
                def attackPlayer():
                    if clickIfExists("1516895180850.png"): 
                        prepareCounterAttack()
                    performActionUntilStop(quickPVPAttack, lambda: not BATTLE_REGION.exists("1516895773203.png"), 5)
                    saveAttackRecord(captureScreenshot(), ".counterattack.png")
                    #self.goBackToClanHome()
    
                pvpRoutine(lambda: attackPlayer())

    def checkMailbox(self):
        # Do not perform check in the mid night.
        now = time.time()
        currTimeInHMS = ((now + TZOFFSET) % DAY_IN_SECS)
        if currTimeInHMS > 0 and currTimeInHMS < (7 * HOUR_IN_SECS): return
        try:
            clickIfExists("1516621296303.png")
            
            waitAndClick("1516621336565.png")
            clickIfExists("1516622496504.png")
            wait(1)
            
        except FindFailed: pass
        finally:
            if clickIfExists("1516621398337.png"):
                waitAndClick("1516621398337.png")

class CharacterStatus:

    def hasPotion(self):
        return True

    def _parseNumber(self, value):
        newValue = ""
        for c in value.strip():
            if c.isdigit(): newValue = newValue + c
            else: newValue = newValue + "0"

        return newValue

    def blood(self):
        rawHealth = Region(141,72,47,23).text()
        log("rawHealth=%s" % str(rawHealth))

        health = self._parseNumber(rawHealth)
        if not health or health.startswith("0"):
            wa1 = health.lstrip("0")
            if len(wa1) >= 3 and len(wa1) <= 4:
                health = wa1
            elif len(health) == 3:
                return 900
            else:
                #print "Unable to proceed(1): %s (%s)" % (health, rawHealth)
                return -1
            
        health = health[:3] if len(health) > 4 else health
        blood = int(health)
        blood = blood / 10 if (blood > 5000) else blood
        return blood

    def manaPoints(self):
        def localParseNumber(value):
            idx = 0
            newValue = ""
            for c in value.strip():
                idx += 1
                if c.isdigit(): newValue = newValue + c
                else: newValue = newValue + "0"
    
            return newValue
        
        rawMana = Region(156,95,30,15).text()
        try:
            mana = self._filterNumber(localParseNumber(rawMana), CHARACTER_MAX_MANA)
            log("rawMana=%s, filtered=%s" % (rawMana, mana))
            if mana > CHARACTER_MAX_MANA:
                return CHARACTER_MAX_MANA
            return int(mana)
        except Exception as e:
            log("An error has occured: %s" % (e))
            return -1

    def _filterNumber(self, numValueStr, maxValue):
        if not numValueStr: return -1
        if len(numValueStr) == 4:
            numValueStr = numValueStr[1:]

#        log("numValueStr=%s, len=%d" % (numValueStr, len(numValueStr)))
        numValueStr = numValueStr[:3] if len(numValueStr) > 4 else numValueStr
        intVal = int(numValueStr)
        return intVal

    def isInSafeArea(self):
        if  Region(1093,352,105,33).exists("1520417262741.png"):
            place = Region(1075,336,122,25)
            exceptionalPlaces = ["1520417288759.png"]
            for exceptional in exceptionalPlaces:
                if place.exists(exceptional):
                    return False
            return True
        return False
            

    def detectIdle(self, idleHandler):
        ON_GOING_REGION = Region(22,258,163,52)
        ON_GOING_ACTION = "1520417331192.png"
        def isTakingAction():
           return ON_GOING_REGION.exists(ON_GOING_ACTION)

        while True:
            checkIfAppExiting()
            sampleTimes = 0
            while not isTakingAction():
                log("detectIdle - no action")
                checkIfAppExiting()
                sampleTimes += 1
                
                if (sampleTimes >= 2):
                    if (idleHandler):
                        idleHandler()
                time.sleep(0.5)

def movingStats(windowSize):
    samples = [[]]
    def sampler(value):
        pass

def forceExecuteAfterNTimesFail(n, name):
    failed = [0]
    def routine(val):
        if (val == -1):
            failed[0]+=1
            if failed[0] < n:
                return False

        failed[0] = 0
        return True

    return routine

def main():
    undetectedTimes = 0

    reactor = GamingReactor()
    character = CharacterStatus()

    hpForceActionGate = forceExecuteAfterNTimesFail(AUTO_HEALTH_RECOVERY_FORCE_TIMES, "hp")
    manaForceActionGate = forceExecuteAfterNTimesFail(5, "mp")
    manaRecoveryThrottler = throttler(AUTO_MANA_RECOVERY_THROTTLER)
    
    while True:
        try:
            if EXCLUSIVE_MODE.get():
                time.sleep(2)
                continue

            t0 = time.time()
            hp = character.blood()
            t1 = time.time()
            mp = character.manaPoints()
            t2 = time.time()
            hasPotion = character.hasPotion()
            log("HP=%d, MP=%d, hasPotion=%s, cost(hp)=%d, cost(mp)=%d" % (hp, mp, str(hasPotion), t1-t0, t2-t1))

            if AUTO_HEALTH_HOTKEY and not hasPotion:
                if not character.hasPotion():
                    reactor.goBackToClanHome()
                    break

            if AUTO_HEALTH_RECOVERY:
                forceRecovery = hpForceActionGate(hp)
                performRecovery = (hp == -1 and forceRecovery) \
                        or (hp > 0 and (hp < SUFFICIENT_HP_THRESHOLD))
                if performRecovery:
                    reactor.performRecovery()
                    log("Perform health recovery: hp=%d, threshold=%d" % (hp, SUFFICIENT_HP_THRESHOLD))                

            if AGGRESIVE_ATTACKING:
                forceAttack = manaForceActionGate(mp) if AGGRESIVE_ATTACKING_FORCE else False
                performAttack = (mp == -1 and forceAttack) \
                        or (mp > SUFFICIENT_MP_THRESHOLD)
                if performAttack:
                    reactor.performAttack()

            if AUTO_MANA_RECOVERY \
                and (mp != -1 and mp <= SUFFICIENT_MP_THRESHOLD):
                if manaRecoveryThrottler():
                    reactor.performManaRecover()

            time.sleep(1)
        except : raise

def isTimeWithInOneHourOfTheSpecifiedTime(specifiedTime):
    if specifiedTime < 0: return False
    hhmmssInSecs = ((time.time() + TZOFFSET) % DAY_IN_SECS) - specifiedTime
    return (hhmmssInSecs > 0 and hhmmssInSecs < HOUR_IN_SECS)
    
def test():
    while True:
        score = detectPatternThenTakeAction(["1516515000700.png"], None, similarity = 0.9)
        print score
        time.sleep(1)

class BackgroundTasks:
    substories = SubStories()
    reactor = GamingReactor()

    @staticmethod
    def checkMailbox():
        BackgroundTasks.reactor.checkMailbox()

    @staticmethod
    def detectPVP():
        reactor = GamingReactor()
        try:
            reactor.escapeIfUnderAttack()
            reactor.performPoisonCureIfNeeded()
            time.sleep(1)
        except: raise

    @staticmethod
    def detect78Guys():
        def detect(pics):
           for pic in pics:
               try:
                   m = find(pic)
                   if exists(pic):
                       print "====BEGIN====="
                       print m
                       print "====END======="
                       return True
               except: pass
           return False
       
        while True:
            try:
                checkIfAppExiting()
                if detect(["1521855896852.png","1521308292231.png"]):
                    exclusiveActions(lambda: \
                        BackgroundTasks.reactor.teleport())
                time.sleep(1)
            except: raise

    @staticmethod
    def autoMoveToSubstory():
        if (TO_ABANDONED_AREA_TIME >= 0):
            if (datetime.now().isoweekday() != MAINTENANCE_DAY \
                    and isTimeWithInOneHourOfTheSpecifiedTime(TO_ABANDONED_AREA_TIME)) \
                    or (datetime.now().isoweekday() == MAINTENANCE_DAY \
                    and isTimeWithInOneHourOfTheSpecifiedTime(TO_ABANDONED_AREA_TIME_ON_MAINTAIN_DAY)):
                BackgroundTasks.substories.goToAbandonedArea()

        if (TO_DREAM_ISLAND_TIME >= 0):
            if (datetime.now().isoweekday() != MAINTENANCE_DAY \
                    and isTimeWithInOneHourOfTheSpecifiedTime(TO_DREAM_ISLAND_TIME)) \
                    or (datetime.now().isoweekday() == MAINTENANCE_DAY \
                    and isTimeWithInOneHourOfTheSpecifiedTime(TO_DREAM_ISLAND_TIME_ON_MAINTAIN_DAY)):
                BackgroundTasks.substories.goToDreamIsland()

    @staticmethod
    def teleportWhenIdle(enabled):
        checkIfAppExiting()
        if not enabled or EXCLUSIVE_MODE.get():
            time.sleep(60)
            return

        character = CharacterStatus()
        try:
            def moveToOtherPlace():
                if not character.isInSafeArea() and not EXCLUSIVE_MODE.get():
                    BackgroundTasks.reactor.teleport()

            performTeleport = not character.isInSafeArea()
            if performTeleport:
                character.detectIdle(lambda: moveToOtherPlace())
        except: raise

    @staticmethod
    def timer(handlerConfigs):
        if not handlerConfigs: return True
        
        def inspectTimeAndTriggerHandlers():
            now = time.time()
            handlers = [x for x in handlerConfigs \
                    if "lastCheckTime" not in x \
                    or (now - x["lastCheckTime"]) > x["timePeriod"]]
            for handler in handlers:
                if not "initRun" in handler \
                        or (not handler["initRun"] and "lastCheckTime" in handler):
                    try:
                        handler["runnable"]()
                    except Exception as e:
                        log("[%s] An error has occured: %s" % (handler, e))
                        checkIfAppExiting()
                handler["lastCheckTime"] = now

        try:
            while True:
                checkIfAppExiting()
                inspectTimeAndTriggerHandlers()
                time.sleep(1)
        except Exception as e:
            log("[%s] An error has occured: %s" % (handlerConfigs, e))
            raise

    @staticmethod
    def backHomeIfPotionExhausted():
        numPotions = Region(952,688,30,15).text()
        log("numPotions=%s" % numPotions)
        
def specialRecovery():
    while FAST_RECOVERY and FAST_RECOVERY_PROC:
        try:
            steps = len(FAST_RECOVERY_PROC["keys"])
            for i in range(steps):
                key = FAST_RECOVERY_PROC["keys"][i]
                #resetDelay()
                pressKey(key) if key.isdigit() else clickIfExists(key)

                time.sleep(FAST_RECOVERY_PROC["delays"][i])
        except:
            #traceback.print_exc(file=sys.stdout)
            raise Exception("Shutdown")
    
def loginRoutine():
    def saveScreen(screenshotFile, suffix=False):
        record = "%s/%s" % (GAME_RECORDS_DIR, os.path.basename(screenshotFile))
        if suffix: attackedRecord = attackedRecord + suffix
        os.rename(screenshotFile, attackedRecord)

    
    def chkIfConfirmNeeded():
        if exists("1520374222755.png"):
            saveScreen(captureScreenshot(), ".counterattack.png")
        
    def chkIfUnderMaintain():
        
        pass        

    APP_REGION = Region(247,27,151,37)
    while not APP_REGION.exists("1520374172893.png"):
        if not clickIfExists("1520374561750.png"): 
            time.sleep(5)
            continue

        while True:
            find("1520374405442.png")


if __name__ == '__main__':
#    loginRoutine()

    AGGRESIVE_MODE = {
        "AUTO_HEALTH_RECOVERY": True,
        "AUTO_MANA_RECOVERY": True,
        "AGGRESIVE_ATTACKING": True,
#        "AUTO_TELEPORT_ENABLED": False,
        "PVP_COUNTER_ATTACK": True,
#        "AUTO_HEALTH_HOTKEY": "6",
        "AUTO_MANA_RECOVERY_HOTKEY": "2",
        "AGGRESIVE_ATTACKING_FORCE": True,
        "ATTACK_PERIOD_SECONDS": 2,
        "AUTO_MANA_RECOVERY_THROTTLER": 60,
        "AUTO_TELEPORT_HOTKEY": "4",
        "SUFFICIENT_HP_THRESHOLD": 1000,
    }
    AGGRESIVE_MODE_CUSTOM = dict(AGGRESIVE_MODE)
    AGGRESIVE_MODE_CUSTOM.update({
#        "AGGRESIVE_ATTACKING": False,
#        "AUTO_MANA_RECOVERY": False,
#        "AUTO_TELEPORT_ENABLED": True,
#        "AUTO_HEALTH_RECOVERY_FORCE_TIMES": 120,
         "AUTO_HEALTH_RECOVERY_FORCE_TIMES": 20,
#        "AGGRESIVE_ATTACKING_FORCE": False,
#        "PVP_COUNTER_ATTACK": False,
#        "PVP_DISABLED": True,
#        "AUTO_MANA_RECOVERY_THROTTLER": 10,

#        "AUTO_HEALTH_HOTKEY": None,
#4        "FAST_RECOVERY": True,
#        "FAST_RECOVERY_PROC": FAST_RECOVERY_MANA,
         "FAST_RECOVERY_PROC": FAST_RECOVERY_BOSS,
        #"FAST_RECOVERY_PROC": FAST_BUY_BOXES,
#        "AUTO_MANA_RECOVERY_THROTTLER": 10,
    })

    NO_PPL_MODE = {
        "PVP_DISABLED": False,
        "AUTO_HEALTH_RECOVERY": True,
        "AUTO_MANA_RECOVERY": False,
        "AUTO_MANA_RECOVERY_HOTKEY": None,
        "AUTO_TELEPORT_ENABLED": False,
        "PVP_COUNTER_ATTACK": False,
        "PVP_UNDER_ATTACK_BACK_HOME": False,
        "AUTO_HEALTH_HOTKEY": None,
        "AUTO_MANA_RECOVERY_HOTKEY": "2",
        "AGGRESIVE_ATTACKING": True,
        "AGGRESIVE_ATTACKING_FORCE": True,
        "ATTACK_PERIOD_SECONDS": 3,
#        "AUTO_HEALTH_RECOVERY_FORCE_TIMES": 40,
        "AUTO_MANA_RECOVERY_THROTTLER": 60,
#        "AUTO_ATTACK_USE_KEY": "1",
    }
    KNIGHT_MODE = {
        "AUTO_HEALTH_RECOVERY": False,
        "AUTO_MANA_RECOVERY": False,
        "AUTO_TELEPORT_ENABLED": False,
        "PVP_COUNTER_ATTACK": True,
        "AUTO_MANA_RECOVERY_THROTTLER": 5,
        "AUTO_HEALTH_HOTKEY": None,
        "AUTO_MANA_RECOVERY_HOTKEY": "2",
        "AGGRESIVE_ATTACKING": False,
        "AGGRESIVE_ATTACKING_FORCE": False,
        "ATTACK_PERIOD_SECONDS": 3,
        "AUTO_HEALTH_RECOVERY_FORCE_TIMES": 20,
    }
    MAGE_MODE = {
        "SUFFICIENT_HP_THRESHOLD": 550,
        "PVP_COUNTER_ATTACK": False,
        "AUTO_MANA_RECOVERY": False,
        "AUTO_MANA_RECOVERY_THROTTLER": 5,
        "AUTO_MANA_RECOVERY_HOTKEY": "2",
        "AUTO_HEALTH_HOTKEY": None,

        "AUTO_HEALTH_RECOVERY": True,
        "AUTO_TELEPORT_ENABLED": True,
        "AGGRESIVE_ATTACKING": True,
        "AGGRESIVE_ATTACKING_FORCE": False,
        "ATTACK_PERIOD_SECONDS": 3,
#        "AUTO_HEALTH_RECOVERY_FORCE_TIMES": 12,
        "AUTO_HEALTH_RECOVERY_FORCE_TIMES": 100,
#        "AUTO_ATTACK_USE_KEY": "1522412748861.png",
        "AUTO_ATTACK_USE_KEY": "1522417293644.png",
#        "AUTO_ATTACK_USE_KEY": "1522420441383.png",
    }
    globals().update(AGGRESIVE_MODE_CUSTOM)
#    globals().update(NO_PPL_MODE)
#    globals().update(MAGE_MODE)
#    globals().update(KNIGHT_MODE)
    specialRecovery()

    TIMERS = [
            {
                "timePeriod": 300,
                "runnable": lambda: clickIfExists("1521478694061.png"),
                "enabled": False
            },
            {
                "timePeriod": 21600,
                "runnable": lambda: clickIfExists("1516895998569.png"),
                "initRun": False,
                "enabled": False
            },
            {
                "timePeriod": 30,
                "runnable": lambda: BackgroundTasks.autoMoveToSubstory(),
                "enabled": False
            },
            {
                "timePeriod": 3600,
                "runnable": lambda: BackgroundTasks.checkMailbox(),
                "initRun": False,
                "enabled": True
            },
    ]
    CANCELLABLES = [
            backgroundThread(lambda: BackgroundTasks.detectPVP(), name="backgroundTasks"),
            backgroundThread(lambda: BackgroundTasks.teleportWhenIdle(AUTO_TELEPORT_ENABLED), name="teleportWhenIdle"),
            backgroundThread(lambda: BackgroundTasks.timer([ x for x in TIMERS if not x["enabled"] is False]), name="timer"),
            backgroundThread(lambda: BackgroundTasks.detect78Guys(), name="detect78Guys"),
#            backgroundThread(lambda: BackgroundTasks.backHomeIfPotionExhausted(), name="backHomeIfPotionExhausted"),
            ]

    initTime = datetime.now()
    try:
        main()
    finally:
        APP_EXITING.set(True)
        [x() for x in CANCELLABLES]
        timeElapased = datetime.now() - initTime
        log("[STOP] duration=%s" % str(timeElapased))
